var app = require("express")();
var cors = require('cors')

const redis = require('redis').createClient('redis://redis:6379');

app.use(cors({
    origin: true,
    credentials: true
}));
app.options('*', cors())

var http = require("http").createServer(app);
var io = require("socket.io")(http);

var usersList = [];


const REDIS_USER_DATA_INDEX = 2;

redis.select(REDIS_USER_DATA_INDEX);

redis.on('connect', function () {
    redis.subscribe('notifications');

    redis.on("message", function (channel, message) {
        messageObject = JSON.parse(message)
        io.in(messageObject.userId).emit('notification', messageObject.message, messageObject.reloadable);
    });

}).on('error', function (error) {
    console.log(error);
});

io.on('connection', function (socket) {
    socket.on('join', function (userId) {
        socket.join(userId);
    });

});

http.listen(5000, function () {
    console.log('listening on *:5000');
});
