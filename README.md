## Installation

1. Create a `.env` file from `.env.dist` and adapt it according to the needs of the application

    ```sh
    $ cp .env.dist .env && nano .env
    ```

2.  Due to an Elasticsearch 6 requirement, we may need to set a host's sysctl option and restart ([More info](https://github.com/spujadas/elk-docker/issues/92)):

    ```sh
    $ sudo sysctl -w vm.max_map_count=262144
    ```

3. Build and run the stack in detached mode (stop any system's ngixn/apache2 service first)

    ```sh
    $ docker-compose build
    $ docker-compose up -d
    ```

4. Get the bridge IP address

    ```sh
    $ docker network inspect bridge | grep Gateway | grep -o -E '[0-9\.]+'
    ```

5. Update your system's hosts file with the IP retrieved in **step 3**

6. Prepare the Symfony application
    1. Update Symfony env variables (*.env*)

        ```
        #...
        DATABASE_URL=mysql://db_user:db_password@mysql:3306/db_name
        #...
        ```

    2. Composer install & update the schema from the container

        ```sh
        $ docker-compose exec php bash
        $ composer install
        $ symfony doctrine:schema:update --force
        ```
7. (Optional) Xdebug: Configure your IDE to connect to port `9001` with key `PHPSTORM`

## Usage

Once all the containers are up, our services are available at:

* Symfony app: `http://symfony.dev:80`
* Mysql server: `symfony.dev:3306`
* Redis: `symfony.dev:6379`
* Elasticsearch: `symfony.dev:9200`
* Kibana: `http://symfony.dev:5601`
* RabbitMQ: `http://symfony.dev:15672`
* Log files location: *logs/nginx* and *logs/symfony*

:tada: Now we can stop our stack with `docker-compose down` and start it again with `docker-compose up -d`
