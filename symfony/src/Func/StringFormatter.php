<?php

namespace App\Func;


class StringFormatter
{
    /**
     * under_score to CamelCase
     *
     * @param string $string needle
     * @return string formattedString
     */
    public static function camelCase($string)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', str_replace(".","_",$string))));
    }
    /**
     * CamelCase to under_score
     *
     * @param string $string needle
     * @return string formattedString
     */
    public static function snakeCase($string)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));
    }

    /**
     * @param $phone
     * @return string
     */
    public static function reformatPhone($phone)
    {
        $phone = preg_replace('~\D~', '', $phone);

        if (strlen($phone) == 11 && $phone[0] == "7") {
            $phone[0] = 8;
        } elseif (strlen($phone) == 10) {
            $phone = "8" . $phone;
        }
        if (empty($phone))
            $phone = null;
        return $phone;
    }
}
