<?php


namespace App\Func;


class FloatOperations
{
    public static function floatDifference($minuend, $subtrahend)
    {
        $minuendPlaces = self::getFloatDecimalPlaces($minuend);
        $subtrahendPlaces = self::getFloatDecimalPlaces($subtrahend);
        $pow = $minuendPlaces >= $subtrahendPlaces ? $minuendPlaces : $subtrahendPlaces;
        $counter = 10 ** $pow;
        $result = ($minuend * $counter - $subtrahend * $counter) / $counter;
        $resultPlaces = self::getFloatDecimalPlaces($result);

        $resultPow = $resultPlaces >= 10 ? 10 : $resultPlaces;

        return number_format($result,$resultPow,'.','');
    }

    public static function getFloatDecimalPlaces($needle)
    {
        $str = rtrim(number_format($needle, intval(14 - log10($needle))), '0');
        return strlen(substr(strrchr($str, "."), 1));
    }
}
