<?php


namespace App\Func;


class DatetimeFunc
{
    public static function getDatetimeIntervals(\DateTime $startDatetime, \DateTime $processDatetime = null, int $period = 30)
    {
        $startDatetime->setTime(0, 0);

        $nowDatetime = $processDatetime ?? new \DateTime('now');

        $diffDays = $nowDatetime->diff($startDatetime)->days;

        $diffDaysDivPeriod = intdiv($diffDays, $period);

        $currentPeriod = $diffDaysDivPeriod * $period;

        $startDatetime->modify("+{$currentPeriod} days");

        $endDatetime = (clone $startDatetime)->modify("+{$period} days");

        return [$startDatetime, $endDatetime];
    }
}